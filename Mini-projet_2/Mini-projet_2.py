# Mini-Projekt 2
import json

def reshape_left_side(left_side):
    """
    This funtions reshapes the variables stored in leftside so that, so 
    that the printing of the ATM should be appropriate.

    Parameters
    ----------
    left_side : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """    
    len_dimensions = []
    length = [] 
    for text in left_side:
        len_dimensions.append(len(text[0]))
        len_dimensions.append(len(text[1]))
        length.append(len_dimensions)
        len_dimensions = []
        
    max_length = max(max(length))
    
    reshape = []
    step = 0
    for text in left_side:
        diff_with_max = []
        diff_with_max.append(max_length - len(text[0]))
        diff_with_max.append(max_length - len(text[1]))
        j = 0
        while j < 2:
            for i in range(0, diff_with_max[j]):
                reshape.append(" ")
                if i == diff_with_max[j] - 1:
                    left_side[step][j] = "".join(reshape) + text[j]
                    reshape = []
                    
            j += 1
                    
        step += 1
        
    return left_side
    

def transfer_fcn(card):
    """
    This funtion transfers Money from one card to another

    Parameters
    ----------
    card : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    rec_acc = input("TRANSFER \n\n" + "Enter receving account: ")
    rec_sum = input("Enter receving amount: ")
    reason = input("Enter reason: ")
    
    with open("Cards_infos.json", "w") as json_file:
        cards["JSON"]["Value"][str(rec_acc)]["Saldo [â‚¬]"] += float(rec_sum)
        cards["JSON"]["Value"][str(card)]["Saldo [â‚¬]"] -= float(rec_sum)
        json_file.write(json.dumps(cards, indent = 4))
 
def balance_fcn(current_card):
    """
    This function displays the current balance of the current card

    Parameters
    ----------
    current_card : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    print("BALANCE\n" + "Your balance is " + str(current_card["Saldo [€]"]))
    
def withdrawal_fcn(card):
    """
    This function redraws Money from an account

    Parameters
    ----------
    card : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    withdra_amt = input("WITHDRAWAL\n\n" + "Enter withdrawal amount: ")
    
    with open("Cards_infos.json", "w") as json_file:
        cards["JSON"]["Value"][str(card)]["Saldo [â‚¬]"] -= float(withdra_amt)
        json_file.write(json.dumps(cards, indent = 4))
    
    print("Amount withdrew: " + str(withdra_amt))
        
def deposit_fcn(card):
    """
    This function deposits money in an account

    Parameters
    ----------
    card : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    withdra_amt = input("WITHDRAWAL\n\n" + "Enter deposit amount: ")
    
    with open("Cards_infos.json", "w") as json_file:
        cards["JSON"]["Value"][str(card)]["Saldo [â‚¬]"] += float(withdra_amt)
        json_file.write(json.dumps(cards, indent = 4))
    
    print("Amount deposited: " + str(withdra_amt))

def pin_change_fcn(card,current_card):
    """
    This function changes the PIN code of a card

    Parameters
    ----------
    card : TYPE
        DESCRIPTION.
    current_card : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    curr_pin = ""
    curr_pin = input("PIN CHANGE \n\n" + "Enter current PIN: ")
    while True:
        if curr_pin == str(current_card["PIN"]):
            new_pin = input("Enter new PIN: ")
            confirm_new_pin = input("Confirm new PIN: ")
            if new_pin == confirm_new_pin:
                with open("Cards_infos.json", "w") as json_file:
                    cards["JSON"]["Value"][str(card)]["PIN"] = int(new_pin)
                    json_file.write(json.dumps(cards, indent = 4))
                    break
            else:
                continue
        else:
            print("Wrong PIN! Retry. \n")
            
    
print("Note before you start that the cards hier represensted  with numbers form 1 to 5 and their details are stored in the file Cards_infos.json \n\n" 
        + "Check the file, if you need more details.\n\n"
        + "Please Enter the required inputs correctly. The conditions checking to look for errors isn't yet implemented. \n\n\n")
        
print("Welcome! \n")       
card = input("Enter Card: ")

current_card = {"IBAN":[], "Saldo [€]": None, "PIN": None}
with open("Cards_infos.json", "r") as json_file:
    cards = json.load(json_file)
    current_card["IBAN"] = cards["JSON"]["Value"][str(card)]["IBAN"]
    current_card["Saldo [€]"] = (cards["JSON"]["Value"][str(card)]["Saldo [â‚¬]"])
    current_card["PIN"] = cards["JSON"]["Value"][str(card)]["PIN"]

print("Choose one Option to continue \n\n")

#Left screen side
transfer = []
transfer.append("TRANSFER FUNDS")
transfer.append("Transfer fund to another account")

balance = []
balance.append("BALANCE ENQUIRY")
balance.append("Check the balance of your account")

statement = []
statement.append("MINI STATEMENT")
statement.append(" Check the statement your account")

mobil_recharge = []
mobil_recharge.append("MOBILE RECHARGE")
mobil_recharge.append("Recharge your mobile number")

left_side = [transfer, balance, statement, mobil_recharge]

reshape_left_side(left_side)

#Right screen side
fast_cash = []
fast_cash.append("FAST CASH")
fast_cash.append("Get fast cash of selected values")

withdrawal = []
withdrawal.append("WITHDRAWAL")
withdrawal.append("Withdraw money from your account")

cash_deposit = []
cash_deposit.append("CASH DEPOSIT")
cash_deposit.append("Deposit cash into your account here")

pin_change = []
pin_change.append("PIN CHANGE")
pin_change.append("Change your ATM/Debit Card PIN")

remove_card = []
boolean_remove = True
remove_card.append("REMOVE CARD")
remove_card.append("Remove your card")

right_side = [fast_cash, withdrawal, cash_deposit, pin_change, remove_card]

step_1 = 4
step_2 = 9
text = ""
for counter in reversed(range(0, len(right_side))):
    if counter < len(left_side):
        text = left_side[counter][1] + "                    " + right_side[counter][1] + "\n\n\n" + text
        text = left_side[counter][0] + ".{}                {}.".format(step_1,step_2) + right_side[counter][0] + "\n" + text 
        step_1 -= 1
        step_2 -= 1
    else:
        text = "                                                     " + right_side[counter][1] + "\n\n\n" + text
        text = "                                                   {}.".format(step_2) + right_side[counter][0] + "\n" + text 
        step_2 -= 1
  
while boolean_remove:
 
    print(text)
    choice = input()

    if choice == "1":
        transfer_fcn(card)
        
    elif choice == "2":
        balance_fcn(current_card)
        
    elif choice == "3":
        print("Not yet implemented!")
        
    elif choice == "4":
        print("Not yet implemented!")
        
    elif choice == "5":
        print("Not yet implemented!")
        
    elif choice == "6":
        withdrawal_fcn(card)
        
    elif choice == "7":
        deposit_fcn(card)
        
    elif choice == "8":
        pin_change_fcn(card,current_card)
        
    elif choice == "9":
        print("REMOVE CARD \n\n" + "Card ejected. Thank you, see you again!")
        boolean_remove = False