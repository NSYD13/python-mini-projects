# -*- coding: utf-8 -*-
"""
Created on Sat May 25 12:56:09 2024

@author: yvand
"""

from IPython import get_ipython
from functions import create_account
from functions import system
import keyboard
import json
import os


def clear_console():
    ipython = get_ipython()
    if ipython is not None:
        ipython.magic('clear')


def logging(u_name, password):
    with open("data/accounts.json", "r") as json_file:
        accounts = json.load(json_file)

        if u_name in accounts["JSON"]["user_name"]:
            if password == accounts["JSON"]["user_name"][u_name]["password"]:
                print("Logging successful!")
                return True

        print("Username or password is incorrect! Retry!")
        return "incorrect"


def login_fcn():

    index = 0
    username = ""
    password = ""

    complete = False
    while not complete:
        if index == 0:
            result, index = create_account.save_keyboard_input("text.screen_login_text(finished_text,'')", index)
            username = result[0]

        elif index == 1:
            result, index = create_account.save_keyboard_input(f"text.screen_login_text('{username}','*' * len("
                                                               f"finished_text))", index)
            password = result[0]
            complete = result[1]

    logged = logging(username, password)

    return (username, password), logged
