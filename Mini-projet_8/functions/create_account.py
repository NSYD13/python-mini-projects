# -*- coding: utf-8 -*-
"""
Created on Sun May 26 09:04:06 2024

@author: yvand
"""

from functions import text
from functions import system
import keyboard
import json
import os


def create_path(path):
    os.makedirs(path, exist_ok=True)


def creating(name, u_name, u_email, u_password, con_password):
    if not (u_password == con_password) and len(u_password) > 0:
        print("Password is not identical. Retry!")
        return 'incorrect'

    else:
        new_account = {"name": name, "email": u_email, "password": u_password}
        with open("data/accounts.json", "r") as json_file:
            accounts = json.load(json_file)
            with open("data/accounts.json", "w") as json_file:
                accounts["JSON"]["user_name"][u_name] = new_account
                json_file.write(json.dumps(accounts, indent=4))
                print("Registry successfull!\n\n"
                      + "You can now login")

        path = f"data/{u_name}"
        create_path(path)
        return True


def create_acc_fcn():
    index = 0
    name = ""
    user_name = ""
    email = ""
    passwort = ""
    confirm_password = ""

    complete = False
    while not complete:
        if index == 0:
            result, index = save_keyboard_input("text.screen_create_text(finished_text, '', '', '', '')", index)
            name = result[0]

        elif index == 1:
            result, index = save_keyboard_input(f"text.screen_create_text('{name}', finished_text, '', '', "
                                                f"'')", index)
            user_name = result[0]

        elif index == 2:
            result, index = save_keyboard_input(f"text.screen_create_text('{name}', '{user_name}', finished_text, '', "
                                                f"'')", index)
            email = result[0]

        elif index == 3:
            result, index = save_keyboard_input(f"text.screen_create_text('{name}', '{user_name}', '{email}', '*' * "
                                                f"len(finished_text), '')", index)
            passwort = result[0]

        elif index == 4:
            result, index = save_keyboard_input(
                f"text.screen_create_text('{name}', '{user_name}', '{email}', '{passwort}', '*' * "
                f"len(finished_text))", index)
            confirm_password = result[0]
            complete = result[1]

    logged = creating(name, user_name, email, passwort, confirm_password)

    return (user_name, passwort), logged


def save_keyboard_input(parameter, counter):
    system.variable_reset()
    system.text_display = parameter
    result = system.get_keyboard_entry()
    if "tab" in result[0]:
        result[0] = result[0].replace("tab", "")

    counter += 1
    system.variable_reset()
    return result, counter


def on_key_event(event):
    global keyboard_input, index, name, username, email, password, confirm_pass, hide_password, hide_password_2, running, logged

    if event.name == 'esc':
        # Event-Handler entfernen, wenn ESC gedrückt wird
        running = False
        logged = False
        print("ESC wurde gedrückt, Programm wird beendet")
        keyboard.unhook_all()
        return

    if event.name == 'enter':
        logged = creating(name, username, email, password, confirm_pass)
        running = False
        keyboard.unhook_all()
        return

    else:
        if event.name == "tab":
            index += 1

        else:
            if event.name == "space":
                keyboard_input[index].append(" ")
            else:
                keyboard_input[index].append(event.name)

            name = "".join(keyboard_input[0])
            username = "".join(keyboard_input[1])
            email = "".join(keyboard_input[2])
            password = "".join(keyboard_input[3])
            confirm_pass = "".join(keyboard_input[4])

            if index == 3:
                hide_password = "*" * len(keyboard_input[3])
            if index == 4:
                hide_password_2 = "*" * len(keyboard_input[4])

    text.screen_create_text(name, username, email, hide_password, hide_password_2)
