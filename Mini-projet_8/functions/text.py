# -*- coding: utf-8 -*-
"""
Created on Mon May 27 13:36:13 2024

@author: yvand
"""


def print_directory(path):
    if path is None:
        print(empty)
    else:
        if len(path) < 10:
            difference = 10 - len(path)
            path.append("\n" * difference)
        print("""
Content:          
        """)
        for line in path:
            print(f"{line}")


def screen_login_text(username, hide_password):
    print(f"""
Login          


                Enter Username: {username}
                Enter Password: {hide_password}

                Press Tab to enter Password (after entering username)
                                Press Enter to Login
                                 Press ESC to exit          
          """)


def screen_create_text(name, username, email, hide_password, hide_password_2):
    print(f"""
Sign in          


                Enter Name:       {name}
                Enter Username:   {username}
                Enter Email:      {email}
                Enter Password:   {hide_password}
                Confirm Password: {hide_password_2}



                Press Tab to enter next parameter
                        Press Enter to Register
                         Press ESC to exit





          """)


def new_filename(filename):
    print(f"""



        Enter filename: {filename}



        """)


def to_delete_filename(filename):
    print(f"""



        Enter filename: {filename}



        """)


welcome_text = """
 
      
       
            Press "l" to Login
                  "s" to Sign in
                  "esc" to exit



                """

closing_text = """
      
      
      
      Program closed!
      
      
      
      
                """

GUI_text = """
                                                        "a"  to logout


           "f" to print directory
           "c" to create new file          
           "d" to delete existing file



            """

not_exist_text = """
      
      
      
      Directory doesn't exist!, new is created!
      
      
      
      """

empty = """



        Empty directory!



        """

valid_extensions = [
    # Text Files
    '.txt', '.md', '.rtf', '.html', '.htm',
    # Document Files
    '.doc', '.docx', '.pdf', '.odt', '.epub', '.xls', '.xlsx', '.csv', '.ppt', '.pptx',
    # Image Files
    '.jpg', '.jpeg', '.png', '.gif', '.bmp', '.svg', '.tif', '.tiff',
    # Audio Files
    '.mp3', '.wav', '.flac', '.aac', '.ogg',
    # Video Files
    '.mp4', '.avi', '.mkv', '.mov', '.wmv', '.flv',
    # Compressed Files
    '.zip', '.rar', '.tar', '.gz', '.7z',
    # Executable Files
    '.exe', '.bat', '.sh', '.py', '.jar',
    # Web Files
    '.html', '.htm', '.css', '.js', '.php', '.asp', '.jsp',
    # Data Files
    '.json', '.xml', '.yaml', '.yml', '.sql', '.db', '.sqlite',
    # Developer Files
    '.c', '.cpp', '.java', '.js', '.py', '.rb', '.php', '.html', '.css',
    # System Files
    '.sys', '.dll', '.ini', '.log', '.cfg'
]

windows_valid_chars = [
    # Small letters
    *'abcdefghijklmnopqrstuvwxyz',
    # Capital letters
    *'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    # Numbers
    *'0123456789',
    # German special characters
    *'äöüßÄÖÜ',
    # Special characters
    ' ', '_', '-', '.'
]

wrong_extension = """



        Wrong extension!



        """

creation_successful = """



        File creation successful!



        """

chosen_not_exist = """



        Chosen file or folder doesn't exist!



        """
