# -*- coding: utf-8 -*-
"""
Created on Tue May 28 00:51:07 2024

@author: yvand
"""

from functions import login
from functions import create_account
from functions import text
import keyboard
import os
import time
import shutil


def print_directory_fcn(path):
    if os.path.exists(path) and os.path.isdir(path):
        data = os.listdir(path)
        if not data:
            return None
        return list(set(data))


def execute_login():
    print(text.welcome_text)
    while True:
        if keyboard.is_pressed('l'):
            login_data, logged = login.login_fcn()
            choice = 'login'
            return [choice, login_data, logged]
        elif keyboard.is_pressed('s'):
            account_data, logged = create_account.create_acc_fcn()
            choice = 'sign_in'
            return [choice, account_data, logged]
        elif keyboard.is_pressed('esc'):
            choice = 'esc'
            logged = False
            return [choice, [], logged]


def execute_choice(parameter):
    if parameter[0] == 'login':
        path = f"data/{parameter[1][0]}"
        if not os.path.exists(path):
            print(text.not_exist_text)
            time.sleep(5)
            create_account.create_path(path)
        print(text.GUI_text)

    elif parameter[0] == 'sign_in':
        print(text.GUI_text)

    elif parameter[0] == 'esc':
        print(text.closing_text)


def on_key_press(event):
    global keyboard_input, running, finished_text, stop, text_display

    if event.name == "enter":
        running = False
        keyboard.unhook_all()
        return

    if event.name == "tab":
        keyboard_input.append("tab")
        finished_text = "".join(keyboard_input)
        running = False
        keyboard.unhook_all()
        return

    elif event.name == "backspace":
        if len(keyboard_input) > 0:
            keyboard_input.pop()

    else:
        if event.name in text.windows_valid_chars:
            if event.name == "space":
                keyboard_input.append("_")

            else:
                keyboard_input.append(event.name)

            finished_text = "".join(keyboard_input)

            exec(text_display)


def get_keyboard_entry():
    global keyboard_input, running, finished_text, stop, text_display

    exec(text_display)

    keyboard.on_press(on_key_press)

    while running:
        pass

    stop = True

    return [finished_text, stop]


def execute_directory(parameter):
    global keyboard_input, running, finished_text, stop, text_display
    complete = False

    while True:

        # Displays file lists for a given directory
        if keyboard.is_pressed('f'):
            path_list = print_directory_fcn(parameter)
            text.print_directory(path_list)
            if path_list is None:
                time.sleep(5)
                print(text.GUI_text)
                continue

            else:
                complete = False

                while not complete:
                    variable_reset()
                    text_display = f"""
text.print_directory({path_list}) 
print('Enter chosen filename/folder: ',finished_text)         
                    """

                    # display directory
                    result = get_keyboard_entry()
                    complete = result[1]
                    variable_reset()

                if result[0] in path_list:
                    if "." in result[0]:

                        # check file extension
                        _, extension = os.path.splitext(result[0])
                        for file_typ in text.valid_extensions:
                            if file_typ == extension:
                                valid = True
                                break
                            else:
                                valid = False

                        if valid:

                            # Open the file with its associated application
                            absolute_path = os.path.abspath(f"{parameter}/{result[0]}")
                            os.startfile(absolute_path)
                            time.sleep(5)
                            print(text.GUI_text)
                            continue

                    else:
                        parameter += f"/{result[0]}"
                        print(text.GUI_text)
                        continue

                else:
                    print(text.chosen_not_exist)
                    time.sleep(5)
                    print(text.GUI_text)
                    continue

        # creates new file
        elif keyboard.is_pressed('c'):
            complete = False

            # retrieve Filename
            while not complete:
                variable_reset()
                text_display = f"text.new_filename(finished_text)"
                result = get_keyboard_entry()
                complete = result[1]
                variable_reset()

            # check file extension
            name, extension = os.path.splitext(result[0])
            if extension == "":
                create_account.create_path(f"{parameter}/{name}")
                print(text.GUI_text)
                continue

            else:
                valid = extension in text.valid_extensions
                if valid:
                    with open(f"{parameter}/{result[0]}", "w"):
                        print(text.creation_successful)
                        time.sleep(5)
                        print(text.GUI_text)
                        variable_reset()
                        continue

                else:
                    print(text.wrong_extension)
                    time.sleep(5)
                    print(text.GUI_text)
                    # variable_reset()
                    continue

        # deletes a given file
        elif keyboard.is_pressed('d'):
            path_list = print_directory_fcn(parameter)
            text.print_directory(path_list)
            if path_list is None:
                time.sleep(5)
                continue

            while not complete:
                variable_reset()
                text_display = f"""
text.print_directory({path_list}) 
print('Enter chosen filename: ',finished_text)         
                                    """
                result = get_keyboard_entry()
                complete = result[1]
                variable_reset()

            # actual deletion
            if result[0] in path_list:
                if "." in result[0]:
                    try:
                        os.remove(f"{parameter}/{result[0]}")
                        print(f"File '{parameter}/{result[0]}' has been deleted successfully.")
                        print(text.GUI_text)
                        continue

                    except FileNotFoundError:
                        print(f"File '{parameter}/{result[0]}' does not exist.")
                        print(text.GUI_text)
                        continue

                else:
                    shutil.rmtree(f"{parameter}/{result[0]}")
                    print(f"File '{parameter}/{result[0]}' has been deleted successfully.")
                    print(text.GUI_text)
                    continue

        elif keyboard.is_pressed('a'):
            break


def variable_reset():
    global keyboard_input, running, finished_text, stop, text_display

    keyboard_input = []
    finished_text = ""
    running = True
    stop = False
    text_display = ""


keyboard_input = []
finished_text = ""
running = True
stop = False
text_display = ""
