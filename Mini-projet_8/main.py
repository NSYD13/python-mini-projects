# -*- coding: utf-8 -*-
"""
Created on Sat May 25 16:16:55 2024

@author: yvand
"""

from functions import text
from functions import system

active = True

while active:
    result = system.execute_login()

    # If login or sign in was successful
    if result[2] == "incorrect":
        pass

    elif result[2]:
        system.execute_choice(result)
        path = f"data/{result[1][0]}"
        system.execute_directory(path)

    # End program
    else:
        active = False
        print(text.closing_text)
