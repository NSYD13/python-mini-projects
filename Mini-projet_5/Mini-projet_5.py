# -*- coding: utf-8 -*-
"""
Created on Sat Jan  6 20:03:18 2024

@author: yvand
"""

import os
import json
import uuid6
import datetime

def print_book(books, uuid, counter):
    if books["JSON"]["Books"][uuid]['copies'] == 0:
        books["JSON"]["Books"][uuid]['copies'] = "sold out"
        
    print("\n{}. Title: {} \n   Author: {} \n   Price [€]: {} \n   Available Copies: {}".format(
        str(counter + 1),
        books["JSON"]["Books"][uuid]['title'],
        books["JSON"]["Books"][uuid]['author'],
        books["JSON"]["Books"][uuid]['price'],
        str(books["JSON"]["Books"][uuid]['copies'])
        )
    )
    
def print_worker(accounts_workers, user_name, counter):
    print("\n{}. Name: {} \n   Email: {} \n   Status: Employee".format(
        str(counter + 1),
        accounts_workers["JSON"]["user_name"][user_name]['name'],
        accounts_workers["JSON"]["user_name"][user_name]['email']
        )
    )
    
def print_admin(accounts_administrators, user_name, counter):
    print("\n{}. Name: {} \n   Email: {} \n   Status: Administrator".format(
        str(counter + 1),
        accounts_administrators["JSON"]["user_name"][user_name]['name'],
        accounts_administrators["JSON"]["user_name"][user_name]['email']
        )
    )
    
def print_successfully(var1: str, var2: str):
   print("{} successfully {}!\n".format(var1, var2))
   
   if var1.lower() == "book" and var2.lower() == "added":
       print("To add a new Book press ""Enter""\n"
             + "To end press any key\n")
          
   
def effectuate_transaction(book, status: str, user_name, user_email):
    with open("transactions.json","r") as json_file:
        transactions = json.load(json_file)
        with open("transactions.josn","w") as json_file:
            transaction_id = str(uuid6.uuid6())
            date = datetime.datetime.now()
            date = date.strftime("%Y-%m-%d %H:%M:%S")
            transaction_data = {"{}".format(transaction_id): {"Book ID": book.uuid,
                                                              "Title": book.title,
                                                              "Author": book.author,
                                                              "Price": book.price,
                                                              "Transaction date": date,
                                                              "Status": "{}".format(status.lower()),
                                                              "User_name": user_name,
                                                              "User_email": user_email
                                                              }
                                }
            if status.lower() == "sold":
                transactions["JSON"]["saldo [â‚¬]"] += book.price
                
            transactions["JSON"]["transactions"].update(transaction_data)
            json_file.write(json.dumps(transactions, indent = 4))
            print_successfully("Transaction", "done!. Thank you")
            
    with open("books.json", "r") as json_file:
        books = json.load(json_file)
        with open("books.json", "w") as json_file:
            if status.lower() == "return":
                books["JSON"]["Books"][book.uuid]["copies"] += 1
            else:
                books["JSON"]["Books"][book.uuid]["copies"] -= 1
                
            json_file.write(json.dumps(books, indent = 4))
    
    
class Books():
    
    def __init__(self, book_title, book_author, book_price: float, copies: int):
        self.title = book_title
        self.author = book_author
        self.price = float(book_price)
        self.num_copies = int(copies)
        self.uuid = str(uuid6.uuid6())
        
    def add_book(self):

        # Bundle Book data in a dictionary
        book_data = {"{}".format(self.uuid): {'title': self.title,
                                              'author': self.author,
                                              'price': self.price,
                                              'copies': self.num_copies
                                              }
                     }

        with open("books.json","r+") as json_file:
            books = json.load(json_file)
            with open("books.json", "w") as json_file:
                books["JSON"]["Books"].update(book_data)
                json_file.write(json.dumps(books, indent = 4))
                print_successfully("Book", "added")
        
    def search_book():
        search_word = input("Enter Search word (Author or Title): ").lower()
        
        if len(search_word) == 0:
            print("Wrong Entry. Give the Entry!")
            
        else:
            check = False
            with open("books.json","r") as json_file:
                books = json.load(json_file)
                counter = 0
                uuids = []
                for uuid in books["JSON"]["Books"]:
                    if search_word in books["JSON"]["Books"][uuid]['title'].lower() or search_word in books["JSON"]["Books"][uuid]['author'].lower():
                        print_book(books, uuid, counter)
                        uuids.append(uuid)
                        check = True
                        counter += 1
                
                if check:
                    while True:
                        choice = int(input()) - 1
                        try:
                            if choice >= 0 and choice < counter:
                                bk = Books.read_book(uuids[choice])  
                                return bk
                        except:
                            print("Nothing")
                
                        
    def delete_all_copies(uuid):
        with open("books.json","r") as json_file:
            books = json.load(json_file)
            with open("books.json","w") as json_file:
                del books["JSON"]["Books"][uuid]
                json_file.write(json.dumps(books, indent = 4))
                print_successfully("All copies", "deleted")

    def delete_one_copy(uuid):
        with open("books.json","r") as json_file:
            books = json.load(json_file)
            with open("books.json","w") as json_file:
                if books["JSON"]["Books"][uuid]['copies'] > 0:
                    books["JSON"]["Books"][uuid]['copies'] -= 1
                    json_file.write(json.dumps(books, indent = 4))
                    print_successfully("Book", "deleted")
                else:
                    print("Sold out!!!")

    def list_all_books():
        with open("books.json","r") as json_file:
            books = json.load(json_file)
            uuids = []
            for counter,uuid in enumerate(books["JSON"]["Books"]):
                print_book(books, uuid, counter)
                uuids.append(uuid)
            
            return counter, uuids
            
    def read_book(uuid):
        with open("books.json", "r") as json_file:
            books = json.load(json_file)
            book_data = books["JSON"]["Books"][uuid]
            title = book_data["title"]
            author = book_data["author"]
            price = book_data["price"]
            copies = book_data["copies"]
            book = Books(title, author, price, copies)
            book.uuid = uuid
            return book
        
    def buy_book(self, user_name, user_email):
        effectuate_transaction(self, "Sold", user_name, user_email)
        print_successfully("Book","bought")
                
    def borrow_book(self, user_name, user_email):
        effectuate_transaction(self, "Borrowed", user_name, user_email)  
        print_successfully("Book","borrowed")
    
    def return_book(self, user_name, user_email):
        effectuate_transaction(self, "Returned", user_name, user_email)  
        print_successfully("Book","return")
                
class Users():

    def __init__(self, firstname, lastname, u_name, email, password):
        self.name = firstname + " " + lastname
        self.email = email
        self.password = password
        self.u_name = u_name
        self.status = "User"
        
    def add_user(self):

        # Bundle users data in a dictionary
        user_data = {"{}".format(self.u_name.lower()): {"name": self.name,
                                                "email": self.email,
                                                "password": self.password,
                                                "status": self.status
                                                }
                       }
        with open("accounts.json", "r+") as json_file:
            accounts = json.load(json_file)
            with open("accounts.json", "w") as json_file:
                accounts["JSON"]["user_name"].update(user_data)
                json_file.write(json.dumps(accounts, indent = 4))
                print_successfully("Member", "added")

    def read_user(user_name):
        with open("accounts.json", "r") as json_file:
            accounts = json.load(json_file)
            user_data = accounts["JSON"]["user_name"][user_name]
            firstname, lastname = user_data["name"].split(" ")
            email = user_data["email"]
            
            if email == "":
                email = " "
                
            password = user_data["password"]
            user = Users(firstname, lastname, user_name, email, password)
            return user
               
class Workers():
    
    def __init__(self, firstname, lastname, email, password):
        self.name = firstname + " " + lastname
        self.email = email
        self.password = password
        self.u_name = firstname.lower() + "." + lastname.lower()
        self.status = "Employee"
        
    def add_worker(self):

        # Bundle workers data in a dictionary
        worker_data = {"{}".format(self.u_name.lower()): {"name": self.name,
                                                "email": self.email,
                                                "password": self.password,
                                                "status": self.status
                                                }
                       }
        with open("accounts_workers.json", "r+") as json_file:
            accounts_workers = json.load(json_file)
            with open("accounts_workers.json", "w") as json_file:
                accounts_workers["JSON"]["user_name"].update(worker_data)
                json_file.write(json.dumps(accounts_workers, indent = 4))
                print_successfully("Member", "added")

    def read_worker(user_name):
        with open("accounts_workers.json", "r") as json_file:
            accounts_workers = json.load(json_file)
            workers_data = accounts_workers["JSON"]["user_name"][user_name]
            firstname, lastname = workers_data["name"].split(" ")
            email = workers_data["email"]
            password = workers_data["password"]
            worker = Workers(firstname, lastname, email, password)
            return worker
         
class Administrators(Workers):
    
    def __init__(self, firstname, lastname, email, password):
        super().__init__(firstname, lastname, email, password)
        self.u_name = firstname.lower() + "." + lastname.lower()
        self.status = "Admin"
        
    def add_worker(self):

        
        # Bundle workers data in a dictionary
        worker_data = {"{}".format(self.u_name.lower()): {"name": self.name,
                                             "email": self.email,
                                             "password": self.password,
                                             "status": self.status
                                             } 
                       }

        with open("accounts_administrators.json","r") as json_file:
            accounts_administrators = json.load(json_file)
            with open("accounts_administrators.json","w") as json_file:
                accounts_administrators["JSON"]["user_name"].update(worker_data)
                json_file.write(json.dumps(accounts_administrators, indent = 4))
                print_successfully("Member", "added")
                
    def del_worker(user_name):
        with open("accounts_workers.json","r") as json_file:
            accounts_workers = json.load(json_file)
            with open("accounts_workers.json","w") as json_file:
                del accounts_workers["JSON"]["user_name"][user_name]
                json_file.write(json.dumps(accounts_workers, indent = 4))
                print_successfully("Member", "deleted")
                
    def read_admin(user_name):
        with open("accounts_administrators.json", "r") as json_file:
            accounts_administrators = json.load(json_file)
            admin_data = accounts_administrators["JSON"]["user_name"][user_name]
            firstname, lastname = admin_data["name"].split(" ")
            email = admin_data["email"]
            password = admin_data["password"]
            admin_d = Administrators(firstname, lastname, email, password)
            return admin_d
        
        
    def list_all_workers():
        with open("accounts_workers.json","r") as json_file:
            accounts_workers = json.load(json_file)
            for counter, user_name in enumerate(accounts_workers["JSON"]["user_name"]):
                print_worker(accounts_workers, user_name, counter)
    

          
def register():
    
    firstname = input("Firstname: ")
    lastname = input("Lastname: ")
    u_name = input("Username: ")
    u_email = input("Enter Useremail: ")
    u_password = input("Enter User password: ")
    con_password = input("Confirm User password: ")
    
    check = firstname == "" and lastname == "" and u_name == "" and u_email == "" and u_password == "" and con_password == ""
    
    while True:
        if u_password == con_password and u_password != "":
            user = Users(firstname, lastname, u_name, u_email, u_password)
            user.add_user()
            print_successfully("Account", "created")
            return
        elif check:
            return
        
        print("Password isnot identical. Retry!\n"
              + "You may also press ""Enter"" everywhere to skip or exit!")
    
def welcome():
    choice = input("\n\n!!WELCOME!! \n\n" 
          + "1. Login \n" 
          + "2. Create an account\n\n")
    
    return choice
    
def what_to_do(book, user_name, user_email):
    while True:
        choice = input("1. Buy the Book\n"
                       + "2. Borrow the book\n"
                       + "Press ""Enter"" to exit!\n")
        if choice == "1":
            book.buy_book(user_name, user_email)
            print_successfully("Book","bought")
            break
            
        elif choice == "2":
            book.borrow_book(user_name, user_email)
            print_successfully("Book","borrowed")
            break
            
        elif choice == "":
            break
            
        else:
            print("Wrong entry, Retry! You may Press Enter to exit!")


def add_new_member():

    firstname = input("Firstname: ")
    lastname = input("Lastname: ")
    email = input("Email: ")
    admin = input("Press ""Enter"" to set this member als admin else press any key.")
    
    # Generates a new password base on current date
    password = datetime.date.today()
    password = datetime.datetime.strftime(password, "%Y%m%d")
    
    worker = Workers(firstname, lastname, email, password)
    
    if admin == "":
        administrator = Administrators(worker)
        administrator.add_worker()
        
    else:
        worker.add_worker()
        
        
def add_new_book():
    while True:
        bk_title = input("Title: ")
        bk_author = input("Author: ")
        
        try:
            bk_price = float(input("Price: "))
            bk_ncopies = int(input("Copies: "))
            break
        except:
            print("Copies: integer and Price: float. Retry!")
    
    # Bundle book data in an Object of class Book
    bk = Books(bk_title, bk_author, bk_price, bk_ncopies)
    
    bk.add_book()
    
def check_if_account_exist(path, username, password):
    found = False
    user = "unknown"
    try:
        with open(path,"r") as json_file:
            accounts = json.load(json_file)
            accounts["JSON"]["user_name"][username]
            if accounts["JSON"]["user_name"][username]["password"] == password:
                if "administrators" in path:
                    admin = Administrators.read_admin(username)
                    print("Logging successful!")
                    found = True
                    return found,admin
                elif "workers" in path:
                    worker = Workers.read_worker(username)
                    found = True
                    print("Logging successful!")
                    return found, worker
                elif path == "accounts.json":
                    user = Users.read_user(username)
                    found = True
                    print("Logging successful!")
                    return found, user
            else:
                return found, user
                    
                    
    except Exception:
        return found, user
    
"""
    main code
"""

while True:
    choice = welcome()
    
    if choice == "1":
           
            username = input("Username: ")
            password = input("Password: ")
            
            if username == "" and password == "":
                break
            
            path = "accounts_administrators.json"
            found, person = check_if_account_exist(path, username, password)
            
            if found == False:
                path = "accounts_workers.json"
                found, person = check_if_account_exist(path, username, password)
                
                if found == False:
                    path = "accounts.json"
                    found, person = check_if_account_exist(path, username, password)
                    
                    if found == True:
                        break
                
                else:
                    break
                
            else:
                break
            
            print("Username or password is incorrect! Retry!\n\n"
                  + "You may also press ""Enter"" to exit." )
    
    elif choice == "2":
        register()
        
    elif choice == "":
        break
    
    else:
        print("Wrong entry! Retry!")
        
        
if choice != "":
    if person.status == "User":
        
        while True:
            choice = input("\n\n1. VIEW ALL AVAIBLE BOOKS\n"
                           + "2. SEARCH FOR A BOOK (Just write anything on the Book)\n"
                           + "3. RETURN A BOOK\n"
                           + "Press ""Enter"" to exit!\n")
                  
            if choice == "1":
                limit, ids = Books.list_all_books()
                while True:
                    try:
                        choice_bk = input("Choose a book, or Press Enter to exit\n")
                        if choice_bk != "":
                            choice_bk = int(choice_bk)
                            
                        if choice_bk >= 0 and choice_bk <= limit:
                            uuid = ids[choice_bk - 1]
                            book = Books.read_book(uuid)
                            what_to_do(book, person.name, person.email)
                            break
                        
                        elif choice_bk == "":
                            break
                            
                        else:
                            print("Wrong entry, Retry! You may Press Enter to exit!")
                            
                    except:
                        print("Wrong entry, Retry! You may Press Enter to exit!")
                break
                
                
            elif choice == "2":
                book = Books.search_book()
                what_to_do(book, person.name, person.email)
                break
                
            elif choice == "3":
                break
            
            elif choice == "":
                break
            
            else:
                print("Wrong entry, Retry! You may Press Enter to exit!")