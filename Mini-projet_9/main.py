from functions import text
from functions import system
from functions import tasks_management as tm
from functions.classes.my_queue import MyQueue as myq

running = True
is_active = True
result = None
logged = None
list_tasks = None

while is_active:
    while running:
        result = system.entry(logged, list_tasks)

        if result[0] == "":
            running = True

        elif result[0] == "esc":
            print(text.closing_text)
            running = False
            is_active = False

        else:
            running = False

    logged = result[2]
    check = (result[0] == "register" or result[0] == "login") and logged is True

    if is_active:
        path = result[1][0]
        q = myq()
        tasks = q.load(path)
        list_tasks = system.tasks_2_text(tasks)

        if check:
            text.print_GUI(list_tasks)
            choice = system.entry(logged, list_tasks)

            if choice == "n":
                q = tm.new_task(result[1][0])

            elif choice == "d":
                q = tm.mark_done(q)
                q.save(path)

            elif choice == 'a':
                del q, tasks, list_tasks, path, check
                result = None
                logged = None
                list_tasks = None
                running = True
                is_active = True
