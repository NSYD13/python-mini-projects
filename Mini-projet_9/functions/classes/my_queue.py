import csv
import os


class MyQueue:
    def __init__(self):
        self._tasks = []

    def done(self):
        if self._tasks:
            pop = self._tasks.pop(0)
            return pop
        else:
            raise IndexError('Queue is empty')

    def put(self, task_name, task_date, task_priority):
        self._tasks.append((task_name, task_date, task_priority))
        self.sort()

    def sort(self):
        self._tasks.sort(key=lambda x: (x[1], x[2], x[0]))

    def size(self):
        return len(self._tasks)

    def is_empty(self):
        return len(self._tasks) == 0

    def save(self, user):
        path = f"data/{user}/tasks.csv"
        with open(path, 'w', newline='') as f:
            writer = csv.writer(f)
            for task in self._tasks:
                writer.writerow(task)

    def load(self, user):
        path = f"data/{user}/tasks.csv"
        with open(path, 'r') as f:
            reader = csv.reader(f)
            self._tasks = []
            for row in reader:
                task_name, task_date, task_priority = row
                self._tasks.append((task_name, task_date, int(task_priority)))
            self.sort()
            tasks = self.get_tasks()
            return tasks

    def get_tasks(self):
        lot = []
        for task_name, task_date, task_priority in self._tasks:
            lot.append((task_name, task_date, task_priority))

        return lot
