from functions import login
from functions import create_account
from functions import text
from functions import tasks_management as tm
import keyboard
import os
import time


def entry(logged, tasks):
    if not logged or logged is None:
        print(text.welcome_text)
        while True:
            if keyboard.is_pressed('l'):
                login_data, logged = login.login_fcn()

                if logged and logged != "incorrect":
                    choice = 'login'

                else:
                    choice = ''

                return [choice, login_data, logged]

            elif keyboard.is_pressed('s'):
                account_data, logged = create_account.create_acc_fcn()

                if logged:
                    choice = 'register'

                else:
                    choice = ''

                return [choice, account_data, logged]

            elif keyboard.is_pressed('esc'):
                choice = 'esc'
                logged = False
                return [choice, [], logged]

    else:
        text.print_GUI(tasks)
        while True:
            if keyboard.is_pressed('n'):
                return 'n'

            elif keyboard.is_pressed('d'):
                return 'd'

            elif keyboard.is_pressed('a'):
                return 'a'


def index_and_choice(parameter, index):
    check = check_option(parameter)
    index += check[0]
    complete = check[1]
    return index, complete


def check_option(parameter):
    if parameter == "tab" or parameter == "enter":
        return [1, False]

    elif parameter == "esc":
        return [0, True]


def tasks_2_text(tasks):
    lo_tasks = ""

    if tasks is None:
        lo_tasks = "Empty"

    else:
        for task, date, priority in tasks:
            if lo_tasks == "":
                lo_tasks = f"{task}\t{date}\t{priority}"

            else:
                lo_tasks += "\n\t" + f"{task}\t{date}\t{priority}"

    return lo_tasks
