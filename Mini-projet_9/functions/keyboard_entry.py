import keyboard


def get_keyboard_entry():
    global letter, running

    reset_variables()

    keyboard.on_press(on_key_press)

    while running:
        pass

    return letter


def on_key_press(event):
    global letter, running, windows_valid_chars

    if event.name == "space":
        event.name = " "

    if event.name in windows_valid_chars:
        letter = event.name

    keyboard.unhook_all()

    running = False


def reset_variables():
    global letter, running

    running = True
    letter = ""


def construct(word):
    active = True
    word = list(word)

    word.append(get_keyboard_entry())

    if word[-1] == "enter":
        word = join_chars(word, "enter")
        return [word, "enter"]

    elif word[-1] == "backspace" or word[-1] == "delete":
        word = word[:-2]
        word = join_chars(word, "")
        return [word, ""]

    elif word[-1] == "tab":
        word = join_chars(word, "tab")
        return [word, "tab"]

    elif word[-1] == "esc":
        word = join_chars(word, "esc")
        return [word, "esc"]

    else:
        word = join_chars(word, "")
        return [word, ""]


def join_chars(parameter, option):
    if option == "enter" or option == "tab" or option == "esc":
        parameter.pop()

    parameter = "".join(parameter)
    return parameter


def enter_the_word(word, to_print):
    option = ""
    while option == "":
        exec(to_print)
        word, option = construct(word)

    return word


windows_valid_chars = [
    # Small letters
    *'abcdefghijklmnopqrstuvwxyz',
    # Capital letters
    *'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
    # Numbers
    *'0123456789',
    # German special characters
    *'äöüßÄÖÜ',
    # Special characters
    ' ', '_', '-', '.',
    # special case
    "enter", "backspace", "tab", "delete", "esc",
]
