from functions import keyboard_entry
from functions import text
from functions import system
import json


def logging(u_name, password):
    with open("data/accounts.json", "r") as json_file:
        accounts = json.load(json_file)

        if u_name in accounts["JSON"]["user_name"]:
            if password == accounts["JSON"]["user_name"][u_name]["password"]:
                print("Logging successful!")
                return True

        print("Username or password is incorrect! Retry!")
        return "incorrect"


def login_fcn():
    index = 0
    username = ""
    password = ""
    option = ""

    complete = False
    while not complete:
        text.screen_login_text(username, '*' * len(password))
        check_enter = len(username) > 0 and len(password) > 0 and option == "enter"

        if check_enter:
            complete = True

        else:
            if index == 0:
                username, option = keyboard_entry.construct(username)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

            elif index == 1:
                password, option = keyboard_entry.construct(password)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

                    if index == 2:
                        index = 0

    logged = logging(username, password)

    return (username, password), logged
