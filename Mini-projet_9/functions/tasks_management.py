from functions import keyboard_entry
from functions import text
from functions import system
from functions.classes.my_queue import MyQueue


def new_task(user):
    index = 0
    task = ""
    date = ""
    priority = ""
    option = ""
    q = MyQueue()
    q.load(user)

    complete = False
    while not complete:
        text.print_new_task(task, date, priority)
        check_enter = len(task) > 0 and len(priority) > 0 and option == "enter"

        if check_enter:
            complete = True

        else:
            if index == 0:
                task, option = keyboard_entry.construct(task)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

            if index == 1:
                date, option = keyboard_entry.construct(date)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

            elif index == 2:
                priority, option = keyboard_entry.construct(priority)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

                    if index == 3:
                        index = 0

    q.put(task, date, priority)
    q.save(user)
    return q


def mark_done(q):
    q.done()
    return q
