from functions import text
from functions import system
from functions import keyboard_entry
import keyword
import json
import os


def create_path(path):
    os.makedirs(path, exist_ok=True)


def creating(name, u_name, u_email, u_password, con_password):
    if not (u_password == con_password) and len(u_password) > 0:
        print("Password is not identical. Retry!")
        return "incorrect"

    else:
        new_account = {"name": name, "email": u_email, "password": u_password}

        if not os.path.exists("data/accounts.json"):
            with open("data/accounts.json", "w") as accounts_file:
                data = {"JSON": {"user_name": {}}}
                accounts_file.write(json.dumps(data, indent=4))

        with open("data/accounts.json", "r+") as json_file:
            accounts = json.load(json_file)
            with open("data/accounts.json", "w") as json_file:
                accounts["JSON"]["user_name"][u_name] = new_account
                json_file.write(json.dumps(accounts, indent=4))
                print("Successfully registered!\n\n"
                      + "You can now login")

        path = f"data/{u_name}"
        create_path(path)
        task_path = os.path.join(path, "tasks.csv")
        with open(task_path, "w") as task_file:
            pass

        return True


def create_acc_fcn():
    index = 0
    name = ""
    u_name = ""
    email = ""
    password = ""
    c_password = ""
    option = ""
    logged = None

    complete = False
    while not complete:
        text.screen_create_text(name, u_name, email, '*' * len(password), '*' * len(c_password))
        check_enter = len(name) > 0 and len(u_name) > 0 and len(password) > 0 and len(c_password) > 0
        check_enter = check_enter and option == "enter"

        if check_enter:
            complete = True

        else:
            if index == 0:
                name, option = keyboard_entry.construct(name)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

                    if check_enter:
                        complete = True

            elif index == 1:
                u_name, option = keyboard_entry.construct(u_name)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

                    if check_enter:
                        complete = True

            elif index == 2:
                email, option = keyboard_entry.construct(email)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

                    if check_enter:
                        complete = True

            elif index == 3:
                password, option = keyboard_entry.construct(password)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

                    if check_enter:
                        complete = True

            elif index == 4:
                c_password, option = keyboard_entry.construct(c_password)

                if option != "":
                    index, complete = system.index_and_choice(option, index)

                    if index == 5:
                        index = 0

                    if check_enter:
                        complete = True

    check_enter = len(name) > 0 and len(u_name) > 0 and len(password) > 0 and len(c_password) > 0
    check_enter = check_enter and option == "enter"

    if check_enter:
        logged = creating(name, u_name, email, password, c_password)

    elif option == "esc":
        logged = False

    return (u_name, password), logged
