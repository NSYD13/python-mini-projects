# -*- coding: utf-8 -*-
"""
Created on Sun Dec 31 12:02:49 2023

@author: yvand
"""


import os
import json
import csv

def register():
    
    name = input("Enter your name: ")
    u_name = input("Enter Username: ")
    u_email = input("Enter Useremail: ")
    u_password = input("Enter User password: ")
    con_password = input("Confirm User password: ")
    
    if not(u_password == con_password):
        print("Password isnot identical. Retry!")
        register()
        
    else:
        new_account = {"name": name, "email": u_email, "password": u_password}
        with open("accounts.json","r") as json_file:
            accounts = json.load(json_file)
            with open("accounts.json", "w") as json_file:
                accounts["JSON"]["user_name"][u_name] = new_account
                json_file.write(json.dumps(accounts, indent = 4))
                print("Registry successfull!\n\n"
                      + "You can now login")
            
        return u_name, u_password
            
def logging(u_name, password):
    
    with open("accounts.json", "r") as json_file:
        accounts = json.load(json_file)
        
        if u_name in accounts["JSON"]["user_name"]:
            if password == accounts["JSON"]["user_name"][u_name]["password"]:
                print("Logging successful!")
                return 
                
        print("Username or password is incorrect! Retry!")
        u_name = input("Username: ")
        password = input("Password: ")
        logging(u_name, password)
                
            
def check_if_exists(file_path):
    
    if not(os.path.isfile(file_path)):
        with open(file_path, "w+") as contacts:
            columns = ["Tasks","Notizen","Date limit\n"]
            contacts.write(",".join(columns))
        
        return
    
def welcome():
    choice = input("\n\n!!WELCOME!! \n\n" 
          + "1. Login \n" 
          + "2. Create an account\n\n")
    
    return choice

def add_task(file_path):
    task = input("TASK: ")
    notice = input("NOTICE: ")
    date_limit = input("Date limit: ")
    
    if len(task) == 0:
        print("Task must not be empty! Retry!")
        add_task(file_path)
    
    else:
        new_task = [task, notice, date_limit + "\n"]
        with open(file_path, "a") as tasks:
            tasks.write(",".join(new_task))
    
def delete_existing_task(file_path):
    
    with open(file_path, "r") as csv_file:
        
        tasks = csv.reader(csv_file)
        tasks_list = list(tasks)
                
        while True:   
            
            for i, line in enumerate(tasks_list):
                if i > 0:
                    print("Task {}: ".format(i) + line[0] 
                          + "\nDate limit: " + line[2]
                          + "\nNotice: " + line[1] + "\n")
                    
            to_delete = input("Note: Use Task index. Also Press Enter when done.\n\n"
                              + "Delete Task: ")
            
            if not(to_delete == ""):
                to_delete = int(to_delete)
                
                if not(to_delete == 0) and to_delete <= i:
                    del tasks_list[to_delete]
                
                else:
                    print("Wrong Entry, Please Retry!")
                    
            else:
                break
                
    with open(file_path, "w", newline = '') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(tasks_list)
        
def mark_as_done(file_path,file_path_2):
    
    with open(file_path, "r") as csv_file:
        
        tasks = csv.reader(csv_file)
        tasks_list = list(tasks)
        tasks_done = []
                
        while True:
            print("Note: Use Task index\n" 
                  + "Press done when you are done marking\n\n")
            
            for i, line in enumerate(tasks_list):
                if i > 0:
                    print("Task {}: ".format(i) + line[0] 
                          + "\nDate limit: " + line[2]
                          + "\nNotice: " + line[1] + "\n")
            
            done = input("Done: ")
            
            if not(done == ""):
                done = int(done)
                tasks_done.append(tasks_list[done])
                del tasks_list[done]
                
            elif int(done < 0) or int(done > i):
                print("Wrong Entry, please Retry!")
                    
                
            else:
                print("All Done!!")
                break
            
        with open(file_path, "w", newline = '') as csv_file_2:
            csv_writer = csv.writer(csv_file_2)
            csv_writer.writerows(tasks_list)
            
        with open(file_path_2, "a", newline = '') as csv_file_3:
            csv_writer = csv.writer(csv_file_3)
            csv_writer.writerows(tasks_done)

def assign_task():
    
    with open("accounts.json", "r") as json_file:
        accounts = json.load(json_file)
        
        while True:
            assign_to = input("Assign to: ")
            
            for u_name in accounts["JSON"]["user_name"]:
                if assign_to == accounts["JSON"]["user_name"][u_name]["name"]:
                    assign_to_uname = u_name
                    file_path = "{}_tasks.csv".format(assign_to_uname)
                    check_if_exists(file_path)
                    file_path_done = "{}_tasks_done.csv".format(assign_to_uname)
                    check_if_exists(file_path_done)
                    
                    add_task(file_path)
                    return
            
            print("This name doesn't exist\n\n"
                  + "Here are the existing names\n")
            print(list(accounts["JSON"]["user_name"][u_name]["name"] for u_name in accounts["JSON"]["user_name"]))
        
"""
    Main Code
"""
    
while True:
    choice = welcome()
    
    if choice == "1":
        u_name = input("Enter Username: ")
        password = input("Enter Password: ")
        logging(u_name, password)
        boolean = True
        break
        
        
    elif choice == "2":
        u_name, password = register()
        
    else:
        print("Wrong Entry, please retry!")
        choice = welcome()

file_path = "{}_tasks.csv".format(u_name)
check_if_exists(file_path)
file_path_done = "{}_tasks_done.csv".format(u_name)
check_if_exists(file_path_done)

while True:
    boolean = False
    while boolean == False:
        choice_2 = input("1. Add a new task\n"
                         + "2. Delete existing task\n"
                         + "3. Mark Task\n"
                         + "4. Assign Task\n\n")
    
        if choice_2 == "1":
            add_task(file_path)
            boolean = True
        elif choice_2 == "2":
            delete_existing_task(file_path)
            boolean = True
        elif choice_2 == "3":
            mark_as_done(file_path, file_path_done)
            boolean = True
        elif choice_2 == "4":
            assign_task()
            boolean = True
        else:
            print("Wrong Entry, please retry!")
        
    print("Operation successfull")
    
    choice_3 = input("Continue (press any key) or Exit (press Enter)\n\n")
    if choice_3 == "":
        break