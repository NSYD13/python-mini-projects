# -*- coding: utf-8 -*-
"""
Created on Fri Dec 22 22:19:54 2023

@author: yvand
"""

import os
import json
import csv

def check_if_exists(file_path):
    
    if not(os.path.isfile(file_path)):
        with open(file_path, "w+") as contacts:
            columns = ["Name","Tel. Number","Email","Notizen\n"]
            contacts.write(",".join(columns))
        
        return

def add_contact(file_path):
    
    name = input("Name: ")
    tel = input("Tel. Number: ")
    email = input("Email: ")
    notice = input("Notice: ")
    
    if len(name) == 0 or len(tel) == 0:
        print("Name or Tel. Number must not be empty! Retry!")
        add_contact(file_path)
        
    else:
        new_contact = [name, tel, email, notice + "\n"]
        with open(file_path, "a") as contacts:
            contacts.write(",".join(new_contact))
            
def change_existing_contact(file_path):
    
    contact_to_change = input("Enter the name of contact to change: ")
    
    with open(file_path, "r") as csv_file:
        
        contacts = csv.reader(csv_file)
        contact_list = list(contacts)
        
        contact_found = []
        contact_index = None
        
        for i, line in enumerate(contact_list):            
            if contact_to_change in line:
                contact_found.append(line)
                contact_index = i
        
        if len(contact_found) > 0:
            name = input("Enter new daten\n\n"
                         + "Name: ")
            tel = input("Tel. Number: ")
            email = input("Email: ")
            notice = input("Notice: ")
            
            changed_daten = [name, tel, email, notice]
            contact_list[contact_index] = changed_daten
            
    with open(file_path, "w", newline = '') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(contact_list)

def register():
    
    name = input("Enter your name: ")
    u_name = input("Enter Username: ")
    u_email = input("Enter Useremail: ")
    u_password = input("Enter User password: ")
    con_password = input("Confirm User password: ")
    
    if not(u_password == con_password):
        print("Password isnot identical. Retry!")
        register()
        
    else:
        new_account = {"name": name, "email": u_email, "password": u_password}
        with open("accounts.json", "w") as json_file:
            accounts["JSON"]["user_name"][u_name] = new_account
            json_file.write(json.dumps(accounts, indent = 4))
            print("Registry successfull!\n\n"
                  + "You can now login")
            
        return u_name, u_password
            
def logging(u_name, password):
    
    with open("accounts.json", "r") as json_file:
        accounts = json.load(json_file)
        
        if u_name in accounts["JSON"]["user_name"]:
            if password == accounts["JSON"]["user_name"][u_name]["password"]:
                print("Logging successful!")
                return 
                
            else:
                print("Username or password is incorrect! Retry!")
                u_name = input("Username: ")
                password = input("Password: ")
                logging(u_name, password)
                
        else:
            print("Username or password is incorrect! Retry!")
            u_name = input("Username: ")
            password = input("Password: ")
            logging(u_name, password)
            
def welcome():
    choice = input("\n\n!!WELCOME!! \n\n" 
          + "1. Login \n" 
          + "2. Create an account\n\n")
    
    return choice
    
"""
    Main Code
"""
with open("accounts.json","r") as json_file:
    accounts = json.load(json_file)
    
boolean = False
while boolean == False:
    choice = welcome()
    
    if choice == "1":
        u_name = input("Enter Username: ")
        password = input("Enter Password: ")
        logging(u_name, password)
        boolean = True
        
        
    elif choice == "2":
        u_name, password = register()
        
    else:
        print("Wrong Entry, please retry!")
        choice = welcome()

file_path = "{}_contacts.csv".format(u_name)
check_if_exists(file_path)

while True:
    boolean = False
    while boolean == False:
        choice_2 = input("1. Add a new contact\n"
                         + "2. Change existing contact\n\n")
    
        if choice_2 == "1":
            add_contact(file_path)
            boolean = True
        elif choice_2 == "2":
            change_existing_contact(file_path)
            boolean = True
        else:
            print("Wrong Entry, please retry!")
        
    print("Operation successfull")
    
    choice_3 = input("Continue (press any key) or Exit (press Enter)\n\n")
    if choice_3 == "":
        break

