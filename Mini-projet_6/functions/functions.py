# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 19:33:35 2024

@author: yvand
"""

import requests

def modify_url(data):
    API_key = 'd01d62e030700e546762224dd1d601de'
    if len(data) == 2:
        url = f'https://api.openweathermap.org/data/2.5/weather?lat={data[0]}&lon={data[1]}&appid={API_key}&units=metric'
        return url
    elif len(data) == 1:
        url = f'https://api.openweathermap.org/data/2.5/weather?q={data}&appid={API_key}&units=metric'
        return url
    else:
        raise ValueError("Wrong input use to call API")
        
def get_response(url):
    response = requests.get(url)
    
    if response.status_code == 200:
        w_data = response.json()
        return w_data
    
def weather_data(data):
        
    url = modify_url(data)
    
    w_data = get_response(url)
    return w_data
        
def get_location():
    response = requests.get('http://ipinfo.io/json')
    data = response.json()
    location = data['loc'].split(',')
    latitude = location[0]
    longitude = location[1]
    return latitude, longitude

def display_weather(data):
    print(f"Current City: {data['name']}\n"
          + f"Temperature: {data['main']['temp']}°C\n"
          + f"Temp_max: {data['main']['temp_max']}°C | Temp_min: {data['main']['temp_min']}°C\n"
          + f"Visibility: {data['visibility']/1000}km\n")