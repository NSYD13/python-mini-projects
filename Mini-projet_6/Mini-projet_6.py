# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 09:01:34 2024

@author: yvand
"""

from functions import functions as func

# Weather of current location
data = func.get_location()

w_data = func.weather_data(data)

func.display_weather(w_data)