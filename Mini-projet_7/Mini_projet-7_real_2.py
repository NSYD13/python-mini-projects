# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 17:25:29 2024

@author: yvand
"""

from functions import *
import mechanicalsoup
import csv
import re


# create stateful browser
browser = mechanicalsoup.StatefulBrowser(
    soup_config = {"features": "lxml"},
    raise_on_404 = True,
    user_agent = "MyBot/0.1: mysite.example.com/bot_info"
    )

# Read Website and go to login_page
url = "https://maze-erp.com/bed/software/app/mobile/index.php"
browser.open(url)
browser.follow_link("login")
form = browser.select_form()
print(form.print_summary())

# Login_daten
browser["mail"] = input("Enter Email")
browser["pwd"] = input("Password")

browser.submit_selected()
current_link = browser.get_url()
print(current_link)

path = [
        "b=3",
        "SOL'S"
        ]

for p in path:
    browser.follow_link(p)


"""
    Article_list
"""
path = ".col-md-3"

article = next_page(path, browser)


"""
    Gender_list
"""

gender_list = ["HOMME",
               "FEMME",
               "ENFANT",
               "UNISEXE"]

result = []
articles_info = []

for i, link in enumerate(article[1]):
    temp_browser = article[2][i]
    temp_browser.open(link)
    
    for gender in gender_list:
        try:
            temp_browser.follow_link(gender)
            result.append(next_page(path, browser))
            temp_browser.open(link)
        except Exception:
            continue
    
        with open(f"{article[0][i]}/{gender}.csv", "w", newline='') as csvfile:
            productswriter = csv.writer(csvfile, delimiter=";", quotechar='"', quoting=csv.QUOTE_MINIMAL)
            
            for j, product in enumerate(result[-1][0]):
                productswriter.writerow([f"{article[0][i]}", product, result[-1][0][i]])
                
    articles_info.append(result)