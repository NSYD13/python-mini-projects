# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 02:55:55 2024

@author: yvand
"""

import mechanicalsoup
import csv
import re

def arrange_text(text):
    while text[0] == "\n":
        del text[0]
        
    while text[-1] == "\n":
        del text[-1]
        
    return "".join(text)

def extract_from_string(text,form):
    if form == "Number":
        numbers = re.findall(r'\d+', text)
        
        if numbers:
            return str(numbers[0])
        else:
            print("Keine Zahl gefunden")
    
    elif form == "Text":
        match = re.search(r"'(.*?)'", text)
        
        if match:
            return match.group(1)
        else:
            print("Kein Text in Anführungszeichen gefunden")


def next_page(path, browser):
    
    page_names = []
    page_links = []
    browser_stand = []
    onclick_details = []
    
    page = browser.get_current_page()
    
    for card in page.select(f'.container .padding-bottom {path}'):
        name = list(card.text)
        test1 = "HOMME" in browser.get_url()
        test2 = "FEMME" in browser.get_url()
        test3 = "ENFANT" in browser.get_url()
        test4 = "UNISEXE" in browser.get_url()
        
        page_names.append(arrange_text(name))
        details = card.attrs["onclick"]
        details_number = extract_from_string(details, "Number")
        if not (test1 or test2 or test3 or test4):
            onclick_details.append(details_number)
            new_link = f"https://maze-erp.com/bed/software/app/mobile/index.php?id_super_family={details_number}"
            
        else:
            command_details = extract_from_string(details, "Text")
            onclick_details.append(f"{command_details}, {details_number}")
            new_link = f"https://maze-erp.com/bed/software/app/mobile/command_models_list.php?product_group_name={command_details}&id_super_family={details_number}"
            
        page_links.append(new_link)
        browser_stand.append(browser)

    return [page_names, page_links, browser_stand, onclick_details]