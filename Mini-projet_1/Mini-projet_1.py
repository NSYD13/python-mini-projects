# Searches and save the sign and it's index of the equation
def find_signs(all_terms):
    counter = 0
    operation_signs = []
    sign_index = []
    for sign in all_terms: 
        if sign == "+" or sign == "-" or sign == "*" or sign == "/":
            operation_signs.append(sign)
            sign_index.append(counter)
        counter += 1
    return [operation_signs,sign_index]

# First operates division and multiplication
def div_or_mul(sign_index,operation_signs,all_terms,sign):
    for step in sign_index:
        if all_terms[step] == sign and sign == "*": 
            new_term = str(float(all_terms[step - 1]) * float(all_terms[step + 1]))
            all_terms[step - 1] = new_term
            del all_terms[step]
            del all_terms[step]
            sign_index.remove(step)
            operation_signs.remove(sign)
            return all_terms
        elif all_terms[step] == sign and sign == "/":
            new_term = str(float(all_terms[step - 1]) / float(all_terms[step + 1]))
            all_terms[step - 1] = new_term
            del all_terms[step]
            del all_terms[step]
            sign_index.remove(step)
            operation_signs.remove(sign)
            return all_terms

# Performs the operation and prints the result
def operation(counter, result,all_terms,operation_signs,step):
    if operation_signs[counter] == "+":
        result = result + float(all_terms[step + 1])
    elif operation_signs[counter] == "-":
        result = result - float(all_terms[step + 1])
    elif operation_signs[counter] == "*":
        result = result * float(all_terms[step + 1])
    elif operation_signs[counter] == "/":
        if float(all_terms[step + 1]) == 0:
            print("Syntax Error")
        else:
            result = result / float(all_terms[step + 1])
            
    return result
    
def operation_in_brackets(all_terms):
    open_index = []
    close_index = []
    sub_terms = []
    counter = 0
    for count_open in all_terms:
        if count_open == "(":
            open_index.append(counter)
        elif count_open == ")" and len(count_open) > 0:
            close_index.append(counter)
            for i in range(open_index[-1] + 1,close_index[0]):
                sub_terms.append(all_terms[i]) 
                
            [operation_signs,sign_index] = find_signs(sub_terms)
            
            while "/" in sub_terms: 
                div_or_mul(sign_index,operation_signs,sub_terms,"/")
                [operation_signs,sign_index] = find_signs(sub_terms)
                
            while "*" in sub_terms:
                div_or_mul(sign_index,operation_signs,sub_terms,"*")
                [operation_signs,sign_index] = find_signs(sub_terms)

            if len(sub_terms) == 1:
                result = float(sub_terms[0])
            else:
                result = 0
                
            counter = 0
            # Performs the operation and prints the result
            for step in sign_index:
                if  step == 1:
                    result = float(sub_terms[counter])
                    result = operation(counter,result,sub_terms,operation_signs,step)

                else:
                    counter += 1
                    result = operation(counter,result,sub_terms,operation_signs,step)

            all_terms[open_index[-1]] = str(result)
            del all_terms[open_index[-1] + 1:close_index[0] + 1]
            open_index.pop()
            close_index.pop()
            return all_terms
        counter += 1
    
    
#    return all_terms

print("Enter equation. NB: all terms are separated by a blank space e.g 4 + 4")
equation = input()

# Converts string "equation" in array of char
preprocess = list(equation)
while " " in preprocess:
    preprocess.remove(" ")

all_terms = []
terms = []        
#for zahl in preprocess:
for term in preprocess:
    if not(term == "+" or term == "-" or term == "*" or term == "/" or term == "(" or term == ")" ):
        terms.append(term)
    else:
        if not(terms == []): 
            all_terms.append("".join(terms))
            all_terms.append(term)
            terms = []
        else:
            all_terms.append(term)

# To append the last term
if not(terms == []):
    all_terms.append("".join(terms))

while "(" in all_terms:    
    all_terms = operation_in_brackets(all_terms)
    

while "/" in all_terms: 
    [operation_signs,sign_index] = find_signs(all_terms)
    div_or_mul(sign_index,operation_signs,all_terms,"/")
    
while "*" in all_terms:
    [operation_signs,sign_index] = find_signs(all_terms)
    div_or_mul(sign_index,operation_signs,all_terms,"*")

result = 0
counter = 0

[operation_signs,sign_index] = find_signs(all_terms)
# Performs the operation and prints the result
for step in sign_index:
    if  step == 1:
        result = float(all_terms[counter])
        result = operation(counter,result,all_terms,operation_signs,step)

    else:
        counter += 1
        result = operation(counter,result,all_terms,operation_signs,step)

if len(sign_index) == 0:
    print(equation + " = " + all_terms[0])
else:
    print(equation + " = " + str(result))